package com.example;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.io.StringReader;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
		CloseableHttpClient client = HttpClients.createDefault();

		HttpResponse response = null;
		try {
			response = client.execute(new HttpGet("http://api.openweathermap.org/data/2.5/weather?id=2172797&mode=xml&appid=bdba677ed66a86153d77ea2793c7b25c&units=metric&lang=pl"));
			int statusCode = response.getStatusLine().getStatusCode();
			HttpEntity entity = response.getEntity();
			String responseString = EntityUtils.toString(entity, "UTF-8");
			System.out.println(responseString);

			TransformerFactory factory = TransformerFactory.newInstance();
			StreamSource xmlStream = new StreamSource("src/main/resources/stylesheet.xsl");

			StreamSource in = new StreamSource(new StringReader(responseString));
			StreamResult out = new StreamResult(System.out);

			Transformer transformer = factory.newTransformer(xmlStream);
			transformer.transform(in, out);
		} catch (IOException | TransformerException e) {
			e.printStackTrace();
		}
	}
}
