<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <h2>Weather Details:</h2>
                <h3>Id miasta: <xsl:value-of select="current/city/@id" /></h3>
                <h3>Nazwa miasta: <xsl:value-of select="current/city/@name" /></h3>
                <h3>Polozenie:</h3>
                <h3>Lon: <xsl:value-of select="current/city/coord/@lon" /></h3>
                <h3>Lat: <xsl:value-of select="current/city/coord/@lat" /></h3>
                <h2>Pogoda:</h2>
                <h3>Wind: <xsl:value-of select="current/wind/speed/@name" />, <xsl:value-of select="current/wind/direction/@name" /></h3>
                <h3>Clouds <xsl:value-of select="current/clouds/@name" /></h3>
                <h3>Ostatnia aktualizacja <xsl:value-of select="current/lastupdate/@value" /></h3>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>